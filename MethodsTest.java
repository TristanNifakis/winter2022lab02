public class MethodsTest{
  public static void main(String[]args){
	  int x = 10;
	  System.out.println(x);
	  methodNoInputReturn();
	  System.out.println(x);
	  
	  methodOneInputReturn(10);
	  methodOneInputReturn(x);
	  methodOneInputReturn(x+5);
	  methodTwoInputNoReturn(9, 8.2);
	  
	  int z = methodNoInputReturnInt();
	  System.out.println(z);
	  
	  double calculationSqrt = sumSquareRoot(6,3);
	  System.out.println(calculationSqrt);
	  
	  String s1 = "hello";
	  String s2 = "goodbye";
	  System.out.println(s1.length());
	  System.out.println(s2.length());
	  
	  System.out.println(SecondClass.addOne(50));
	  SecondClass sc = new SecondClass();
	  System.out.println(sc.addTwo(50));
	  
	  
	  
	  
	  
		
  }
	  
	  public static void methodNoInputReturn() {
		int x = 50;
		System.out.println("Inside the method one input no return");
		System.out.println(x);
	  }
	   public static void methodOneInputReturn(int youCanCallThisWhateverYouLike) {
		System.out.println("I'm in a static method that takes no input and returns nothing");
	  }
	  
	  
	  public static void methodTwoInputNoReturn(int first, double second) {
		  System.out.println("lol");
	  }
	  public static int methodNoInputReturnInt() {
		  
		  return 6;
	  }
	  public static double sumSquareRoot(int x,int y) {
		  double sum = Math.sqrt(x+y);
		  
		  return sum;
	  }
  
}