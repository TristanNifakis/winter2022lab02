import java.util.Scanner;
public class PartThree{
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		System.out.println("What is the length of the square?");
		int lengthS = reader.nextInt();
		
		System.out.println("What is the length of the rectangle?");
		int lengthR = reader.nextInt();
		
		System.out.println("What is the width of the rectangle?");
		int widthR = reader.nextInt();
		
		System.out.println("Area square: "+AreaComputations.areaSquare(lengthS));
		AreaComputations ac = new AreaComputations();
		System.out.println("Area rectangle: "+ac.areaRectangle(lengthR, widthR));
		
	}
 
}